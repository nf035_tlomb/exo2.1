## Homogénéisation : Thibault Lombardo
Opération seffectuées:
- [X] Suppression dees répertoires lib et src, ainsi que leur contenu
- [X] Déplacement des ressources présentes à la racine du projet dans l'arborescence du sous-projet
- [X] S'assurer que tous les tests sont en **JUnit**, ainsi que les **assertions**.
- [X] Tester de nouveau le build ainsi que l'exécution de l'ensemble des tests en une opération sur l'IDE (Eclipse ou autre choix), puis apporter la preuve que tout est "vert" avec une saisie d'écran.


## Screenshot REbuid :
![échantillon][rebuild]

[rebuild]: rebuild.png "rebuild"

## Screenshot Tests passed :
![échantillon][testpassed]

[testpassed]: testpassed.png "testpassed"
